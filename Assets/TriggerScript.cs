﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour {

    Animator animator;
    public GameObject door;
	// Use this for initialization
	void Start () {
        animator = door.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("Kurwa");
            animator.SetBool("Close", false);

            animator.SetBool("Open", true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("lol");
            animator.SetBool("Open", false);

            animator.SetBool("Close", true);
        }
    }

}
